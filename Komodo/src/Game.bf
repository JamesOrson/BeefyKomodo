using Komodo.Core.ECS.Systems;
using System.Collections;
using System;
using Komodo.Core.Engine.Graphics;
using Komodo.Core.Engine.Content;
using Komodo.Core.Engine.Input;
using Komodo.Core.ECS.Components;
using Komodo.Core.ECS;
using Komodo.Core.Engine.Event;

namespace Komodo
{
    /// <summary>
    /// Manages all graphics initialization, systems, and underlying interactions with the MonoGame framework.
    /// </summary>
    public class Game
    {
        #region Public

        #region Constructors
        /// <summary>
        /// Creates the Game instance, instantiating the underlying <see cref="Komodo.Core.MonoGame"/> instance, <see cref="Komodo.Core.Engine.Graphics.GraphicsManager"/>, and <see cref="Komodo.Core.ECS.Systems.ISystem"/> objects.
        /// </summary>
        public this()
        {
            this.GraphicsManager = new GraphicsManager(this)
            {
                IsMouseVisible = true
            };
			this.EventManager = new SDLEventManager();

			this.EntityStore = new Dictionary<uint, Entity>();
			this.EntityComponentStore = new Dictionary<uint, List<Component>>();
	
			this.ContentManager = new ContentManager(this);
            this.BehaviorSystem = new BehaviorSystem(this);
            this.CameraSystem = new CameraSystem(this);
            this.PhysicsSystems = new List<PhysicsSystem>();
            this.Render2DSystems = new List<Render2DSystem>();
            this.Render3DSystems = new List<Render3DSystem>();
            this.SoundSystem = new SoundSystem(this);
			DefaultSpriteShader = new Shader()
			{
			    TextureEnabled = true,
			    VertexColorEnabled = true,
			};

			Game.Instance = this;
        }
        #endregion

        #region Members
        /// <summary>
        /// Manages all <see cref="Komodo.Core.ECS.Components.BehaviorComponent"/> objects.
        /// </summary>
        public BehaviorSystem BehaviorSystem { get; }

        /// <summary>
        /// Manages all <see cref="Komodo.Core.ECS.Components.CameraComponent"/> objects.
        /// </summary>
        public CameraSystem CameraSystem { get; }

		/// <summary>
		/// Provides access to the content files compiled by the MonoGame Content Pipeline (Releases: https://github.com/MonoGame/MonoGame/releases).
		/// </summary>
		public ContentManager ContentManager { get; }

        /// <summary>
        /// Default shader for all <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/>.
        /// </summary>
        public Shader DefaultSpriteShader { get; set; }

		/// <summary>
		/// Maps entity IDs to a list of the registered Components
		/// </summary>
		public Dictionary<uint, List<Component>> EntityComponentStore { get; }

		public Dictionary<uint, Entity> EntityStore { get; }

		public EventManager EventManager { get; }

        /// <summary>
        /// Tracks the FPS based on delta from <see cref="Update()"/>.
        /// </summary>
        public float FramesPerSecond { get; private set; }

        /// <summary>
        /// Manages graphics devices for the Game window.
        /// </summary>
        public GraphicsManager GraphicsManager { get; }

        /// <summary>
        /// Whether or not the game is the current window in focus.
        /// </summary>
        /// <remarks>
        /// Can be used to know whether or not inputs should be dropped while user is not in game.
        /// </remarks>
        public bool IsActive => IsActive;

		public bool IsExiting { get; private set; }

        /// <summary>
        /// Manages all <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> objects.
        /// </summary>
        public List<PhysicsSystem> PhysicsSystems { get; }

        /// <summary>
        /// Manages all <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> objects.
        /// </summary>
        public List<Render2DSystem> Render2DSystems { get; }

        /// <summary>
        /// Manages all <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> objects.
        /// </summary>
        public List<Render3DSystem> Render3DSystems { get; }

        /// <summary>
        /// Name of the current screen device.
        /// </summary>
        public String ScreenDeviceName => Window.ScreenDeviceName;

        /// <summary>
        /// Manages all <see cref="Komodo.Core.ECS.Components.SoundComponent"/> objects.
        /// </summary>
        public SoundSystem SoundSystem { get; }

        /// <summary>
        /// Window title.
        /// </summary>
        public String Title
        {
            get
            {
                return Window?.Title;
            }
            set
            {
                if (Window != null)
                {
                    Window.Title = value;
                }
            }
        }

		public RenderWindow Window { get; set; }
        #endregion

        #region Member Methods

        /*#region Add event handlers
        /// <summary>
        /// Adds a handler to be executed when game is exiting.
        /// </summary>
        /// <param name="handler">Handler to be executed.</param>
        public void AddExitingEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Exiting += handler;
        }
        
        /// <summary>
        /// Adds a handler to be executed when game is exiting.
        /// </summary>
        /// <param name="handler">Handler to be executed.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<EventArgs> AddExitingHandler(Action<object, EventArgs> handler)
        {
            let eventHandler = new EventHandler<EventArgs>(handler);
            AddExitingEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be executed when game gains window focus.
        /// </summary>
        /// <param name="handler">Handler to be executed.</param>
        public void AddFocusGainedEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Activated += handler;
        }

        /// <summary>
        /// Adds a handler to be executed when game gains window focus.
        /// </summary>
        /// <param name="handler">Handler to be executed.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<EventArgs> AddFocusGainedHandler(Action<object, EventArgs> handler)
        {
            let eventHandler = new EventHandler<EventArgs>(handler);
            AddFocusGainedEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be executed when game loses window focus.
        /// </summary>
        /// <param name="handler">Handler to be executed.</param>
        public void AddFocusLostEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Deactivated += handler;
        }

        /// <summary>
        /// Adds a handler to be executed when game loses window focus.
        /// </summary>
        /// <param name="handler">Handler to be executed.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<EventArgs> AddFocusLostHandler(Action<object, EventArgs> handler)
        {
            let eventHandler = new EventHandler<EventArgs>(handler);
            AddFocusLostEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be executed when key is pressed down.
        /// </summary>
        /// <remarks>
        /// Provides access to the <see cref="Microsoft.Xna.Framework.Input.Keys"/> representation of the key pressed down.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        public void AddKeyDownEventHandler(EventHandler<InputKeyEventArgs> handler)
        {
            _monoGame.Window.KeyDown += handler;
        }
        
        /// <summary>
        /// Adds a handler to be executed when key is pressed down.
        /// </summary>
        /// <remarks>
        /// Provides access to the <see cref="Microsoft.Xna.Framework.Input.Keys"/> representation of the key pressed down.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<InputKeyEventArgs> AddKeyDownHandler(Action<object, InputKeyEventArgs> handler)
        {
            let eventHandler = new EventHandler<InputKeyEventArgs>(handler);
            AddKeyDownEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be executed when key is released.
        /// </summary>
        /// <remarks>
        /// Provides access to the <see cref="Microsoft.Xna.Framework.Input.Keys"/> representation of the key released.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        public void AddKeyUpEventHandler(EventHandler<InputKeyEventArgs> handler)
        {
            _monoGame.Window.KeyUp += handler;
        }
        
        /// <summary>
        /// Adds a handler to be executed when key is released.
        /// </summary>
        /// <remarks>
        /// Provides access to the <see cref="Microsoft.Xna.Framework.Input.Keys"/> representation of the key released.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<InputKeyEventArgs> AddKeyUpHandler(Action<object, InputKeyEventArgs> handler)
        {
            let eventHandler = new EventHandler<InputKeyEventArgs>(handler);
            AddKeyUpEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be executed when screen device name changes, so normally if the screen device is changed.
        /// </summary>
        /// <remarks>
        /// Does not provide access to the new screen device name. This can be retrieved from <see cref="ScreenDeviceName"/>.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        public void AddScreenDeviceNameChangedEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Window.ScreenDeviceNameChanged += handler;
        }
        
        /// <summary>
        /// Adds a handler to be executed when screen device name changes, so normally if the screen device is changed.
        /// </summary>
        /// <remarks>
        /// Does not provide access to the new screen device name. This can be retrieved from <see cref="ScreenDeviceName"/>.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<EventArgs> AddScreenDeviceNameChangedHandler(Action<object, EventArgs> handler)
        {
            let eventHandler = new EventHandler<EventArgs>(handler);
            AddScreenDeviceNameChangedEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be run when text is input.
        /// </summary>
        /// <remarks>
        /// Provides access to the printable character and the <see cref="Microsoft.Xna.Framework.Input.Keys"/> representation of the key.
        /// </remarks>
        /// <param name="handler">Handler to be executed when a key is pressed. <c>object</c> provides access to the Window object the event was sent from. <c>args</c> provides access to the pressed key.</param>
        public void AddTextInputEventHandler(EventHandler<TextInputEventArgs> handler)
        {
            _monoGame.Window.TextInput += handler;
        }
        
        /// <summary>
        /// Adds a handler to be run when text is input.
        /// </summary>
        /// <remarks>
        /// Provides access to the printable character and the <see cref="Microsoft.Xna.Framework.Input.Keys"/> representation of the key.
        /// </remarks>
        /// <param name="handler">Handler to be executed when a key is pressed. <c>object</c> provides access to the Window object the event was sent from. <c>args</c> provides access to the pressed key.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<TextInputEventArgs> AddTextInputHandler(Action<object, TextInputEventArgs> handler)
        {
            let eventHandler = new EventHandler<TextInputEventArgs>(handler);
            AddTextInputEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be executed when window changes size.
        /// </summary>
        /// <remarks>
        /// Does not provide access to the new window size. This can be retrieved from <see cref="GraphicsManager.ViewPort"/>.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        /// <returns>Reference to the registered <see cref="System.EventHandler"/>. Can be used to remove the registered handler.</returns>
        public EventHandler<EventArgs> AddWindowSizeChangedHandler(Action<object, EventArgs> handler)
        {
            _monoGame.Window.AllowUserResizing = true;
            let eventHandler = new EventHandler<EventArgs>(handler);
            AddWindowSizeChangedEventHandler(eventHandler);
            return eventHandler;
        }

        /// <summary>
        /// Adds a handler to be executed when window changes size.
        /// </summary>
        /// <remarks>
        /// Does not provide access to the new window size. This can be retrieved from <see cref="GraphicsManager.ViewPort"/>.
        /// </remarks>
        /// <param name="handler">Handler to be executed.</param>
        public void AddWindowSizeChangedEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Window.ClientSizeChanged += handler;
        }
        #endregion*/

        /*#region Remove event handlers
        /// <summary>
        /// Removes a previously registered handler for when game is exiting.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveExitingEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Exiting -= handler;
        }

        /// <summary>
        /// Removes a previously registered handler for when game gaining window focus.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveFocusGainedEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Activated -= handler;
        }

        /// <summary>
        /// Removes a previously registered handler for when game loses window focus.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveFocusLostEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Deactivated -= handler;
        }

        /// <summary>
        /// Removes a previously registered handler for when key is pressed down.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveKeyDownEventHandler(EventHandler<InputKeyEventArgs> handler)
        {
            _monoGame.Window.KeyDown -= handler;
        }

        /// <summary>
        /// Removes a previously registered handler for when key is released.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveKeyUpEventHandler(EventHandler<InputKeyEventArgs> handler)
        {
            _monoGame.Window.KeyUp -= handler;
        }
        
        /// <summary>
        /// Removes a previously registered handler for when screen device name changes, so normally if the screen device is changed.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveScreenDeviceNameChangedEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Window.ScreenDeviceNameChanged -= handler;
        }

        /// <summary>
        /// Removes a previously registered handler for text input.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveTextInputEventHandler(EventHandler<TextInputEventArgs> handler)
        {
            _monoGame.Window.TextInput -= handler;
        }

        /// <summary>
        /// Removes a previously registered handler for when window changes size.
        /// </summary>
        /// <param name="handler">Handler to be removed.</param>
        public void RemoveWindowSizeChangedEventHandler(EventHandler<EventArgs> handler)
        {
            _monoGame.Window.ClientSizeChanged -= handler;
        }
        #endregion*/

		/// <summary>
		/// Adds a <see cref="Komodo.Core.ECS.Components.Component"/> to the relevant <see cref="Komodo.Core.ECS.Systems.ISystem"/>. <see cref="AddComponent(Component)"/> will also remove the <see cref="Komodo.Core.ECS.Components.Component"/> from any Entity and <see cref="Komodo.Core.ECS.Systems.ISystem"/> it was attached to before.
		/// </summary>
		/// <param name="component"><see cref="Komodo.Core.ECS.Components.Component"/> to add.</param>
		/// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.Component"/> was added to this Entity's <see cref="Components"/>. Returns false if the <see cref="Komodo.Core.ECS.Components.Component"/> already existed.</returns>
		public bool AddComponent(Component component)
		{
			let parentID = component.Parent.ID;
			if (!IsEntityRegistered(parentID))
			{
				return false;
			}
			let parent = EntityStore[parentID];
			var components = EntityComponentStore[parentID];
		    if (components.Contains(component))
		    {
		        return false;
		    }
		    components.Add(component);
		    if (component is BehaviorComponent)
			{
		        return BehaviorSystem.[Friend]AddComponent((BehaviorComponent)component);
			}
			else if (component is CameraComponent)
			{
				return CameraSystem.[Friend]AddComponent((CameraComponent)component);
			}
			else if (component is SoundComponent)
			{
			    return SoundSystem.[Friend]AddComponent((SoundComponent)component);
			}
		    else if (component is Drawable2DComponent)
			{
				if (parent.Render2DSystem == null)
				{
					return false;
				}
				return parent.Render2DSystem.[Friend]AddComponent((Drawable2DComponent)component);
			}
		    else if (component is Drawable3DComponent)
			{
				if (parent.Render3DSystem == null)
				{
					return false;
				}
				return parent.Render3DSystem.[Friend]AddComponent((Drawable3DComponent)component);
			}
		    else if (component is PhysicsComponent)
			{
				if (parent.PhysicsSystem == null)
				{
					return false;
				}
				return parent.PhysicsSystem.[Friend]AddComponent((PhysicsComponent)component);
			}
			return false;
		}

		public bool AddEntity(Entity entity)
		{
			let id = entity.ID;
			if (IsEntityRegistered(id))
			{
			   return false;
			}
			EntityStore[id] = entity;
			EntityComponentStore[id] = new List<Component>();
			entity.[Friend]Game = this;
			return true;
		}

        /// <summary>
        /// Creates and begins tracking a new <see cref="Komodo.Core.ECS.Systems.PhysicsSystem"/>.
        /// </summary>
        public PhysicsSystem CreatePhysicsSystem()
        {
            let system = new PhysicsSystem(this);
            PhysicsSystems.Add(system);
            return system;
        }

        /// <summary>
        /// Creates and begins tracking a new <see cref="Komodo.Core.ECS.Systems.Render2DSystem"/>.
        /// </summary>
        public Render2DSystem CreateRender2DSystem()
        {
            let system = new Render2DSystem(this);
            Render2DSystems.Add(system);
            return system;
        }

        /// <summary>
        /// Creates and begins tracking a new <see cref="Komodo.Core.ECS.Systems.Render3DSystem"/>.
        /// </summary>
        public Render3DSystem CreateRender3DSystem()
        {
            let system = new Render3DSystem(this);
            Render3DSystems.Add(system);
            return system;
        }

        /// <summary>
        /// Draws a frame with a default clear color.
        /// </summary>
        /// <param name="delta">Time passed since last <see cref="Draw(delta)"/>.</param>
        public void Draw(float dt)
        {
            FramesPerSecond = (float)(1 / dt);
            Draw(dt, Color.DarkOliveGreen);
        }

        /// <summary>
        /// Draws a frame with a provided clear color.
        /// </summary>
        /// <param name="_">Time passed since last <see cref="Draw(delta)"/>.</param>
        /// <param name="clearColor"><see cref="Microsoft.Xna.Framework.Color"/> to clear the screen with.</param>
        public void Draw(float _, Color clearColor)
        {
            GraphicsManager.BeginDraw(clearColor);

            // 3D must render before 2D or else the 2D sprites will fail to render in the Z dimension properly
            for (let system in Render3DSystems)
            {
                system.Draw();
            }
            let spriteBatch = GraphicsManager.SpriteBatch;
            for (let system in Render2DSystems)
            {
                system.Draw(spriteBatch);
            }
			GraphicsManager.EndDraw();
        }

        /// <summary>
        /// Exits the application.
        /// </summary>
        public void Exit()
        {
            IsExiting = true;
        }

		/// <summary>
		/// Removes an individual <see cref="Komodo.Core.ECS.Components.Component"/> from this Entity's <see cref="Components"/>.
		/// </summary>
		/// <param name="component"><see cref="Komodo.Core.ECS.Components.Component"/> to remove.</param>
		/// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.Component"/> was removed from this Entity's <see cref="Components"/>. Will return false if the <see cref="Komodo.Core.ECS.Components.Component"/> is not present in <see cref="Components"/>.</returns>
		public bool RemoveComponent(Component component)
		{
			if (component.Parent == null)
			{
				return false;
			}
		    let parentID = component.Parent.ID;
			if (!IsEntityRegistered(parentID))
			{
				return false;
			}
			let parent = EntityStore[parentID];
	        if (component is BehaviorComponent)
			{
				if (!BehaviorSystem.[Friend]RemoveComponent((BehaviorComponent)component))
				{
					return false;
				}
			}
	        else if (component is CameraComponent)
			{
				let render2DSystem = parent.Render2DSystem;
				let render3DSystem = parent.Render3DSystem;
	            if (render2DSystem.ActiveCamera == component)
	            {
	                render2DSystem.ActiveCamera = null;
	            }
	            if (render3DSystem.ActiveCamera == component)
	            {
	                render3DSystem.ActiveCamera = null;
	            }
	            if (!CameraSystem.[Friend]RemoveComponent((CameraComponent)component))
	            {
	                return false;
	            }
			}
	        else if (component is Drawable2DComponent)
			{
				let render2DSystem = parent.Render2DSystem;
	            if (render2DSystem != null)
	            {
	                if (!render2DSystem.[Friend]RemoveComponent((Drawable2DComponent)component))
	                {
	                    return false;
	                }
	            }
			}
	        else if (component is Drawable3DComponent)
			{
				let render3DSystem = parent.Render3DSystem;
	            if (render3DSystem != null)
	            {
	                if (!render3DSystem.[Friend]RemoveComponent((Drawable3DComponent)component))
	                {
	                    return false;
	                }
	            }
			}
	        else if (component is PhysicsComponent)
			{
				let physicsSystem = parent.PhysicsSystem;
	            if (physicsSystem != null)
	            {
	                if (!physicsSystem.[Friend]RemoveComponent((PhysicsComponent)component))
	                {
	                    return false;
	                }
	            }
			}
	        else if (component is SoundComponent)
			{	
	            if (!SoundSystem.[Friend]RemoveComponent((SoundComponent)component))
	            {
	                return false;
	            }
			}
			else
			{
				return false;
			}
	        component.Parent = null;
			if (EntityComponentStore[parentID].Remove(component))
			{
				delete component;
				return true;
			}
			return false;
		}

		public bool RemoveEntity(Entity entity)
		{
			return RemoveEntity(entity.ID);
		}

		public bool RemoveEntity(uint id)
		{
			if (!IsEntityRegistered(id))
			{
				return false;
			}
			let components = EntityComponentStore[id];
			while (components.Count > 0)
			{
				if (!RemoveComponent(components.Back))
				{
					return false;
				}
			}
			delete components;
			EntityComponentStore.Remove(id);

			let entity = EntityStore[id];
			EntityStore.Remove(id);
			delete entity;

			return true;
		}

        public void Run()
        {
            Console.WriteLine("Starting up the game...");
			Initialize();

			var thing = TimeSpan.TicksPerSecond;
			thing = DateTime.Now.Ticks;
			var lastFrameTime = DateTime.Now.Ticks / (double)TimeSpan.TicksPerSecond;
			var currentFrameTime = lastFrameTime;
			while (!IsExiting)
			{
				currentFrameTime = DateTime.Now.Ticks / (double)TimeSpan.TicksPerSecond;
				let delta = currentFrameTime - lastFrameTime;
				FramesPerSecond = (float)(1d / delta);
				lastFrameTime = currentFrameTime;
				Update((float)delta);
				Draw((float)delta);
			}
			Console.WriteLine("Stopping the game...");
        }

        /// <summary>
        /// Updates <see cref="Komodo.Core.ECS.Systems.ISystem"/> objects.
        /// </summary>
        /// <param name="delta">Time passed since last <see cref="Update(delta)"/>.</param>
        public void Update(float delta)
        {
			// The order of these updates matters greatly. It defines the behavior of the game application as a whole.
			EventManager.Update();
            InputManager.Update();

            BehaviorSystem.PreUpdate(delta);
            CameraSystem.PreUpdate(delta);
            SoundSystem.PreUpdate(delta);
            for (let system in PhysicsSystems)
            {
                system.PreUpdate(delta);
            }

            BehaviorSystem.Update(delta);
            CameraSystem.Update(delta);
            SoundSystem.Update(delta);
            for (let system in PhysicsSystems)
            {
                system.Update(delta);
            }

            BehaviorSystem.PostUpdate(delta);
            CameraSystem.PostUpdate(delta);
            SoundSystem.PostUpdate(delta);
            for (let system in PhysicsSystems)
            {
                system.PostUpdate(delta);
            }
        }
        #endregion

        #endregion

		#region Public

		#region Member Methods
		/// <summary>
		/// Initializes the <see cref="Komodo.Core.Engine.Graphics.GraphicsManager"/> and all <see cref="Komodo.Core.ECS.Systems.ISystem"/> objects.
		/// </summary>
		private void Initialize()
		{
			Console.WriteLine("Setting up the game...");
		    GraphicsManager.Initialize();
			if (!GraphicsManager.IsInitialized)
			{
				Exit();
				return;
			}
		    GraphicsManager.VSync = false;

		    CameraSystem.Initialize();
		    SoundSystem.Initialize();

		    for (let system in PhysicsSystems)
		    {
		        system.Initialize();
		    }

		    for (let system in Render3DSystems)
		    {
		        system.Initialize();
		    }

		    for (let system in Render2DSystems)
		    {
		        system.Initialize();
		    }

		    BehaviorSystem.Initialize();
		}

		private bool IsEntityRegistered(Entity entity)
		{
			return IsEntityRegistered(entity.ID);
		}

		private bool IsEntityRegistered(uint id)
		{
			return EntityStore.ContainsKey(id);
		}
		#endregion

		#endregion

		public static Game Instance { get; private set; }

		~this()
		{
			Console.WriteLine("Tearing down the game...");

			for (let id in EntityStore.Keys)
			{
				RemoveEntity(id);
			}
			delete EntityStore;
			delete EntityComponentStore;

			delete DefaultSpriteShader;

			delete SoundSystem;

			for (let system in Render3DSystems)
			{
				delete system;
			}
			delete Render3DSystems;

			for (let system in Render2DSystems)
			{
				delete system;
			}
			delete Render2DSystems;

			for (let system in PhysicsSystems)
			{
				delete system;
			}
			delete PhysicsSystems;

			delete CameraSystem;

			delete BehaviorSystem;

			delete Window;

			delete ContentManager;

			delete EventManager;

			delete GraphicsManager;
		}
    }
}