namespace Komodo.Core
{
	public static class IDHelper
	{
		private static uint NextComponentID { get; set; }
		private static uint NextEntityID { get; set; }

		private static uint GetNextComponentID()
		{
			return NextComponentID++;
		}

		private static uint GetNextEntityID()
		{
			return NextEntityID++;
		}
	}
}
