using Komodo.Core.ECS.Components;
using System.Collections;

namespace Komodo.Core.ECS.Systems
{
	public class PhysicsSystem : ISystem<PhysicsComponent>
	{
		public this(Game game) {
			Game = game;
			_uninitializedComponents = new Queue<PhysicsComponent>();
			Components = new List<PhysicsComponent>();
		}

		public List<PhysicsComponent> Components
		{
			get
			{
				return default;
			}
			set;
		}

		public Game Game
		{
			get
			{
				return default;
			}
			private set;
		}

		public bool IsInitialized
		{
			get
			{
				return default;
			}
		}

		public bool IsUpdated
		{
			get
			{
				return default;
			}
			set;
		}

		public void Initialize()
		{

		}

		public void PostUpdate(float delta)
		{

		}

		public void PreUpdate(float delta)
		{
			IsUpdated = false;
		}

		public void Update(float delta)
		{
			if (IsUpdated)
			{
				return;
			}
			for (let component in Components)
			{
				component.Update(delta);
			}
			IsUpdated = true;
		}

		/// <summary>
		/// Tracks all potentially uninitialized <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> objects.
		/// All <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> objects will be initialized in the <see cref="Initialize"/>, <see cref="PreUpdate(GameTime)"/>, or <see cref="PostUpdate(GameTime)"/> methods.
		/// </summary>
		private Queue<PhysicsComponent> _uninitializedComponents { get; }

		private bool AddComponent(PhysicsComponent component)
		{
			if (!component.IsInitialized)
			{
			    _uninitializedComponents.Enqueue(component);
			}
			return AddPhysicsComponent(component);
		}

		/// <summary>
		/// Adds a <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> to relevant <see cref="Components"/>. If the <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> is not initialized, it will be queued for initialization.
		/// </summary>
		/// <param name="componentToAdd"><see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> to add.</param>
		/// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> was added to this BehaviorSystem's <see cref="Components"/>. Returns false if the <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> already existed.</returns>
		private bool AddPhysicsComponent(PhysicsComponent component)
		{
		    if (Components.Contains(component))
		    {
		        return false;
		    }
		    Components.Add(component);
		    return true;
		}

		private bool RemoveComponent(PhysicsComponent component)
		{
			return Components.Remove(component);
		}

		public ~this()
		{
			delete Components;
			delete _uninitializedComponents;
		}
	}
}
