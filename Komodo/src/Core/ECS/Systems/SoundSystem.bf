using Komodo.Core.ECS.Components;
using System.Collections;

namespace Komodo.Core.ECS.Systems
{
	public class SoundSystem : ISystem<SoundComponent>
	{
		public this(Game game) {
			Game = game;
			_uninitializedComponents = new Queue<SoundComponent>();
			Components = new List<SoundComponent>();
		}

		public List<SoundComponent> Components
		{
			get
			{
				return default;
			}
			set;
		}

		public Game Game
		{
			get
			{
				return default;
			}
			private set;
		}

		public bool IsInitialized
		{
			get
			{
				return default;
			}
		}

		public bool IsUpdated
		{
			get
			{
				return default;
			}
			set;
		}

		public void Initialize()
		{

		}

		public void PostUpdate(float delta)
		{

		}

		public void PreUpdate(float delta)
		{
			IsUpdated = false;
		}

		public void Update(float delta)
		{
			if (IsUpdated)
			{
				return;
			}
			for (let component in Components)
			{
				component.Update(delta);
			}
			IsUpdated = true;
		}

		/// <summary>
		/// Tracks all potentially uninitialized <see cref="Komodo.Core.ECS.Components.SoundComponent"/> objects.
		/// All <see cref="Komodo.Core.ECS.Components.SoundComponent"/> objects will be initialized in the <see cref="Initialize"/>, <see cref="PreUpdate(GameTime)"/>, or <see cref="PostUpdate(GameTime)"/> methods.
		/// </summary>
		private Queue<SoundComponent> _uninitializedComponents { get; }

		private bool AddComponent(SoundComponent component)
		{
			if (!component.IsInitialized)
			{
			    _uninitializedComponents.Enqueue(component);
			}
			return AddSoundComponent(component);
		}

		/// <summary>
		/// Adds a <see cref="Komodo.Core.ECS.Components.SoundComponent"/> to relevant <see cref="Components"/>. If the <see cref="Komodo.Core.ECS.Components.SoundComponent"/> is not initialized, it will be queued for initialization.
		/// </summary>
		/// <param name="componentToAdd"><see cref="Komodo.Core.ECS.Components.SoundComponent"/> to add.</param>
		/// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.SoundComponent"/> was added to this BehaviorSystem's <see cref="Components"/>. Returns false if the <see cref="Komodo.Core.ECS.Components.SoundComponent"/> already existed.</returns>
		private bool AddSoundComponent(SoundComponent component)
		{
		    if (Components.Contains(component))
		    {
		        return false;
		    }
		    Components.Add(component);
		    return true;
		}

		private bool RemoveComponent(SoundComponent component)
		{
			return Components.Remove(component);
		}

		public ~this()
		{
			delete Components;
			delete _uninitializedComponents;
		}
	}
}
