using Komodo.Core.ECS.Components;
using System.Collections;

namespace Komodo.Core.ECS.Systems
{
	public class Render3DSystem : ISystem<Drawable3DComponent>
	{
		public this(Game game) {
			Game = game;
			_uninitializedComponents = new Queue<Drawable3DComponent>();
			Components = new List<Drawable3DComponent>();
		}

		public CameraComponent ActiveCamera { get; set; }

		public List<Drawable3DComponent> Components
		{
			get
			{
				return default;
			}
			set;
		}

		public Game Game
		{
			get
			{
				return default;
			}
			private set;
		}

		public bool IsInitialized
		{
			get
			{
				return default;
			}
		}

		public bool IsUpdated
		{
			get
			{
				return default;
			}
			set;
		}

		public void Draw()
		{

		}

		public void Initialize()
		{

		}

		/// <summary>
		/// Tracks all potentially uninitialized <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> objects.
		/// All <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> objects will be initialized in the <see cref="Initialize"/>, <see cref="PreUpdate(GameTime)"/>, or <see cref="PostUpdate(GameTime)"/> methods.
		/// </summary>
		private Queue<Drawable3DComponent> _uninitializedComponents { get; }

		private bool AddComponent(Drawable3DComponent component)
		{
			if (!component.IsInitialized)
			{
			    _uninitializedComponents.Enqueue(component);
			}
			return AddDrawable3DComponent(component);
		}

		/// <summary>
		/// Adds a <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> to relevant <see cref="Components"/>. If the <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> is not initialized, it will be queued for initialization.
		/// </summary>
		/// <param name="componentToAdd"><see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> to add.</param>
		/// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> was added to this BehaviorSystem's <see cref="Components"/>. Returns false if the <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> already existed.</returns>
		private bool AddDrawable3DComponent(Drawable3DComponent component)
		{
		    if (Components.Contains(component))
		    {
		        return false;
		    }
		    Components.Add(component);
		    return true;
		}

		private bool RemoveComponent(Drawable3DComponent component)
		{
			return Components.Remove(component);
		}

		public ~this()
		{
			delete Components;
			delete _uninitializedComponents;
		}
	}
}
