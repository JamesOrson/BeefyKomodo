using System.Collections;
using Komodo.Core.ECS.Components;

namespace Komodo.Core.ECS.Systems
{
	/// <summary>
	/// Defines the interface for all <see cref="Komodo.Core.ECS.Systems"/> classes.
	/// </summary>
	public interface ISystem<T> where T : Component
	{
	    #region Members

	    /// <summary>
	    /// All tracked <see cref="T"/> objects.
	    /// </summary>
	    List<T> Components { get; }

	    /// <summary>
	    /// Reference to current <see cref="Komodo.Core.Game"/> instance.
	    /// </summary>
	    Game Game { get; }

	    /// <summary>
	    /// Whether or not the ISystem has called <see cref="Initialize()"/>.
	    /// </summary>
	    bool IsInitialized { get; }

	    #endregion

	    #region Member Methods
	    void Initialize();

	    #endregion
	}
}
