using Komodo.Core.ECS.Components;
using System.Collections;
using Komodo.Core.Engine.Graphics;

namespace Komodo.Core.ECS.Systems
{
	public class Render2DSystem : ISystem<Drawable2DComponent>
	{
		public this(Game game) {
			Game = game;
			_uninitializedComponents = new Queue<Drawable2DComponent>();
			Components = new List<Drawable2DComponent>();
		}

		public CameraComponent ActiveCamera { get; set; }

		public List<Drawable2DComponent> Components
		{
			get
			{
				return default;
			}
			set;
		}

		public Game Game
		{
			get
			{
				return default;
			}
			private set;
		}

		public bool IsInitialized
		{
			get
			{
				return default;
			}
		}

		public bool IsUpdated
		{
			get
			{
				return default;
			}
			set;
		}

		public void Draw(SpriteBatch spriteBatch)
		{

		}

		public void Initialize()
		{

		}

		/// <summary>
		/// Tracks all potentially uninitialized <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> objects.
		/// All <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> objects will be initialized in the <see cref="Initialize"/>, <see cref="PreUpdate(GameTime)"/>, or <see cref="PostUpdate(GameTime)"/> methods.
		/// </summary>
		private Queue<Drawable2DComponent> _uninitializedComponents { get; }

		private bool AddComponent(Drawable2DComponent component)
		{
			if (!component.IsInitialized)
			{
			    _uninitializedComponents.Enqueue(component);
			}
			return AddDrawable2DComponent(component);
		}

		/// <summary>
		/// Adds a <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> to relevant <see cref="Components"/>. If the <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> is not initialized, it will be queued for initialization.
		/// </summary>
		/// <param name="componentToAdd"><see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> to add.</param>
		/// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> was added to this BehaviorSystem's <see cref="Components"/>. Returns false if the <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> already existed.</returns>
		private bool AddDrawable2DComponent(Drawable2DComponent component)
		{
		    if (Components.Contains(component))
		    {
		        return false;
		    }
		    Components.Add(component);
		    return true;
		}

		private bool RemoveComponent(Drawable2DComponent component)
		{
			return Components.Remove(component);
		}

		public ~this()
		{
			delete Components;
			delete _uninitializedComponents;
		}
	}
}
