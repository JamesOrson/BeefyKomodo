using Komodo.Core.ECS.Components;
using System.Collections;

namespace Komodo.Core.ECS.Systems
{
	public class CameraSystem : ISystem<CameraComponent>
	{
		public this(Game game) {
			Game = game;
			_uninitializedComponents = new Queue<CameraComponent>();
			Components = new List<CameraComponent>();
		}

		public List<CameraComponent> Components
		{
			get
			{
				return default;
			}
			set;
		}

		public Game Game
		{
			get
			{
				return default;
			}
			private set;
		}

		public bool IsInitialized
		{
			get
			{
				return default;
			}
		}

		public bool IsUpdated
		{
			get
			{
				return default;
			}
			set;
		}

		public void Initialize()
		{

		}

		public void PostUpdate(float delta)
		{

		}

		public void PreUpdate(float delta)
		{
			IsUpdated = false;
		}

		public void Update(float delta)
		{
			if (IsUpdated)
			{
				return;
			}
			for (let component in Components)
			{
				component.Update(delta);
			}
			IsUpdated = true;
		}

		/// <summary>
		/// Tracks all potentially uninitialized <see cref="Komodo.Core.ECS.Components.CameraComponent"/> objects.
		/// All <see cref="Komodo.Core.ECS.Components.CameraComponent"/> objects will be initialized in the <see cref="Initialize"/>, <see cref="PreUpdate(GameTime)"/>, or <see cref="PostUpdate(GameTime)"/> methods.
		/// </summary>
		private Queue<CameraComponent> _uninitializedComponents { get; }

		private bool AddComponent(CameraComponent component)
		{
			if (!component.IsInitialized)
			{
			    _uninitializedComponents.Enqueue(component);
			}
			return AddCameraComponent(component);
		}

		/// <summary>
		/// Adds a <see cref="Komodo.Core.ECS.Components.CameraComponent"/> to relevant <see cref="Components"/>. If the <see cref="Komodo.Core.ECS.Components.CameraComponent"/> is not initialized, it will be queued for initialization.
		/// </summary>
		/// <param name="componentToAdd"><see cref="Komodo.Core.ECS.Components.CameraComponent"/> to add.</param>
		/// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.CameraComponent"/> was added to this BehaviorSystem's <see cref="Components"/>. Returns false if the <see cref="Komodo.Core.ECS.Components.CameraComponent"/> already existed.</returns>
		private bool AddCameraComponent(CameraComponent component)
		{
		    if (Components.Contains(component))
		    {
		        return false;
		    }
		    Components.Add(component);
		    return true;
		}

		private bool RemoveComponent(CameraComponent component)
		{
			return Components.Remove(component);
		}

		public ~this()
		{
			delete Components;
			delete _uninitializedComponents;
		}
	}
}
