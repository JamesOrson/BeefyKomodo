namespace Komodo.Core.ECS.Components
{
	public abstract class BehaviorComponent : Component
	{
		public abstract void Update(float delta);
	}
}
