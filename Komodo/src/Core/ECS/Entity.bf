using System;
using System.Collections;
using Komodo.Lib.Math;
using Komodo.Core.ECS.Components;
using Komodo.Core.ECS.Systems;

namespace Komodo.Core.ECS
{
	/// <summary>
	/// Represents a collection of <see cref="Komodo.Core.ECS.Components.Component"/> objects tracked by the relevant <see cref="Komodo.Core.ECS.Systems.ISystem"/> objects.
	/// </summary>
	public class Entity
	{
	    #region Public

	    #region Constructors
	    /// <param name="game">Reference to current <see cref="Komodo.Core.Game"/> instance.</param>
	    public this()
	    {
	        ID = IDHelper.[Friend]GetNextEntityID();
	        Components = new List<Component>();
	        IsEnabled = true;
	        Position = Vector3.Zero;
	        Rotation = Vector3.Zero;
	        Scale = Vector3.One;
	    }
	    #endregion

	    #region Members
		/// <summary>
		/// <see cref="Komodo.Core.ECS.Components.CameraComponent"/> to be used for rendering all tracked <see cref="Components"/>.
		/// </summary>
		public CameraComponent ActiveCamera { get; set; }

	    /// <summary>
	    /// All tracked <see cref="Komodo.Core.ECS.Components.Component"/> objects.
	    /// </summary>
	    public List<Component> Components { get; private set; }

	    /// <summary>
	    /// Reference to current <see cref="Komodo.Core.Game"/> instance.
	    /// </summary>
	    public Game Game { get; private set; }

	    /// <summary>
	    /// Unique identifier for the Entity.
	    /// </summary>
	    public uint ID { get; private set; }

	    /// <summary>
	    /// Whether or not the Entity and all child Components should be drawn or updated.
	    /// </summary>
	    public bool IsEnabled { get; set; }

	    /// <summary>
	    /// Defines what <see cref="Komodo.Core.ECS.Systems.PhysicsSystem"/> should be used to simulate <see cref="Komodo.Core.ECS.Components.PhysicsComponent"/> objects. This allows for <see cref="Komodo.Core.ECS.Components.PhsyicsComponent"/> objects to simulate specific subsets of all Entity objects.
	    /// </summary>
	    public PhysicsSystem PhysicsSystem { get; set; }

	    /// <summary>
	    /// Position in world space. Used by <see cref="Komodo.Core.ECS.Components.Component"/> objects to determine their world space.
	    /// </summary>
	    public Vector3 Position { get; set; }

	    /// <summary>
	    /// Defines what <see cref="Komodo.Core.ECS.Systems.Render2DSystem"/> should be used to render <see cref="Komodo.Core.ECS.Components.Drawable2DComponent"/> objects. This allows for <see cref="Komodo.Core.ECS.Components.CameraComponent"/> objects to render specific subsets of all Entity objects.
	    /// </summary>
	    public Render2DSystem Render2DSystem { get; set; }

	    /// <summary>
	    /// Defines what <see cref="Komodo.Core.ECS.Systems.Render3DSystem"/> should be used to render <see cref="Komodo.Core.ECS.Components.Drawable3DComponent"/> objects. This allows for <see cref="Komodo.Core.ECS.Components.CameraComponent"/> objects to render specific subsets of all Entity objects.
	    /// </summary>
	    public Render3DSystem Render3DSystem { get; set; }
	    
	    /// <summary>
	    /// Rotation in world space.
	    /// </summary>
	    public Vector3 Rotation { get; set; }

	    /// <summary>
	    /// Rotation in world space as a <see cref="Microsoft.Xna.Framework.Matrix"/>.
	    /// </summary>
	    public Matrix RotationMatrix => Matrix.CreateFromYawPitchRoll(Rotation.Y, Rotation.X, Rotation.Z);

	    /// <summary>
	    /// Rotation in world space as a <see cref="Microsoft.Xna.Framework.Quaternion"/>.
	    /// </summary>
	    public Quaternion RotationQuaternion => Quaternion.CreateFromYawPitchRoll(Rotation.Y, Rotation.X, Rotation.Z);
	    
	    /// <summary>
	    /// Scaling for the entire entity. Scales all child <see cref="Komodo.Core.ECS.Components.Component"/> objects.
	    /// </summary>
	    public Vector3 Scale { get; set; }
	    #endregion

	    #region Member Methods
	    /// <summary>
	    /// Adds a <see cref="Komodo.Core.ECS.Components.Component"/> to the relevant <see cref="Komodo.Core.ECS.Systems.ISystem"/>. <see cref="AddComponent(Component)"/> will also remove the <see cref="Komodo.Core.ECS.Components.Component"/> from any Entity and <see cref="Komodo.Core.ECS.Systems.ISystem"/> it was attached to before.
	    /// </summary>
	    /// <param name="component"><see cref="Komodo.Core.ECS.Components.Component"/> to add.</param>
	    /// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.Component"/> was added to this Entity's <see cref="Components"/>. Returns false if the <see cref="Komodo.Core.ECS.Components.Component"/> already existed.</returns>
	    public bool AddComponent(Component component)
	    {
			if (component.Parent != null)
			{
				if (component.Parent.ID == this.ID)
				{
					return false;
				}
				Game.RemoveComponent(component);
			}
			component.Parent = this;
	        return Game.AddComponent(component);
	    }

	    /// <summary>
	    /// Removes an individual <see cref="Komodo.Core.ECS.Components.Component"/> from this Entity's <see cref="Components"/>.
	    /// </summary>
	    /// <param name="component"><see cref="Komodo.Core.ECS.Components.Component"/> to remove.</param>
	    /// <returns>Whether or not the <see cref="Komodo.Core.ECS.Components.Component"/> was removed from this Entity's <see cref="Components"/>. Will return false if the <see cref="Komodo.Core.ECS.Components.Component"/> is not present in <see cref="Components"/>.</returns>
	    public bool RemoveComponent(Component component)
	    {
			if (component.Parent == null || component.Parent.ID != this.ID)
			{
				return false;
			}
	        return Game.RemoveComponent(component);
	    }
	    #endregion

	    #endregion

		public ~this()
		{
			for (let component in Components)
			{
				delete component;
			}
			delete Components;
		}
	}
}
