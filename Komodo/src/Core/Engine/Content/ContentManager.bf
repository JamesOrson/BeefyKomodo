namespace Komodo.Core.Engine.Content
{
	public class ContentManager
	{
		public this(Game game) {
			Game = game;
		}

		public Game Game { get; private set; }
	}
}
