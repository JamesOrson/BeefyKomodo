namespace Komodo.Core.Engine.Graphics
{
	public class GraphicsManager
	{
		public this(Game game) {
			Game = game;
			Window = new SDLRenderWindow("Title", 640, 480);
			SpriteBatch = new SpriteBatch();
		}

		public Game Game { get; private set; }
		public bool IsMouseVisible { get; set; }
		public SpriteBatch SpriteBatch { get; }
		public bool VSync { get; set; }
		public RenderWindow Window { get; }

		public bool IsInitialized { get; private set; }

		public void BeginDraw(Color clearColor)
		{

		}

		public void Initialize()
		{
			if (Window.Initialize() == .Err)
			{
				return;
			}

			IsInitialized = true;
		}

		public void EndDraw()
		{
			Window.Blit();
		}

		public ~this()
		{
			delete SpriteBatch;
			delete Window;
		}
	}
}
