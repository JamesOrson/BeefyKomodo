using SDL2;
using System;
using Vulkan;
using System.Collections;

namespace Komodo.Core.Engine.Graphics
{
	class SDLRenderWindow : RenderWindow
	{
		#region Public

		#region Constructors
		public this(String title, uint width, uint height) : base(title, width, height)
		{
		}
		#endregion

		#region Member Methods
		public override void Blit()
		{
			SDL.RenderClear(_renderer);

			uint32 format = ?;
			int32 access = ?;
			int32 w = ?;
			int32 h = ?;
			SDL.QueryTexture(_screenTexture, out format, out access, out w, out h);

			for (int32 i = 0; i < 4; ++i)
			{
				for (int32 j = 0; j < 4; ++j)
				{
					var rect = SDL.Rect(j * w, i * h, w, h);
					SDL.RenderCopy(_renderer, _screenTexture, null, &rect);
				}
			}

			SDL.RenderPresent(_renderer);
		}

		public override Result<bool> Initialize()
		{
			if (SDL.Init(SDL.InitFlag.Video) < 0)
			{
				return HandleSDLError();
			}
#if BF_PLATFORM_WINDOWS
			SDL.VERSION(out _windowInfo.version);
#endif

			_window = SDL.CreateWindow(
				Title,
				SDL.WindowPos.Centered,
				SDL.WindowPos.Centered,
				(int32)Width,
				(int32)Height,
				SDL.WindowFlags.Shown
#if VULKAN
				| SDL.WindowFlags.Vulkan
#else
				| SDL.WindowFlags.OpenGL
#endif
				
			);
			if (_window == null)
			{
				return HandleSDLError();
			}

#if VULKAN
			uint32 extensionCount = ?;
			SDL.Vulkan_GetInstanceExtensions(_window, out extensionCount, null);
			
			var extensionNames = new List<char8*>(extensionCount);
			for (var i = 0; i < extensionCount; ++i)
			{
				extensionNames.Add("");
			}

			SDL.Vulkan_GetInstanceExtensions(_window, out extensionCount, extensionNames.Ptr);

			_instanceCreateInfo.pApplicationInfo = &_appInfo;
			_instanceCreateInfo.enabledLayerCount = 0;
			_instanceCreateInfo.ppEnabledLayerNames = null;
			_instanceCreateInfo.enabledExtensionCount = extensionCount;
			_instanceCreateInfo.ppEnabledExtensionNames = extensionNames.Ptr;

			delete extensionNames;

			if (Vulkan.vkCreateInstance(&_instanceCreateInfo, null, &_instance) != .Success)
			{
				return HandleVulkanError();
			}

#if BF_PLATFORM_WINDOWS
			_winSurfaceCreateInfo = Vulkan.Win32.Win32SurfaceCreateInfoKHR();
			if (!SDL.GetWindowWMInfo(_window, ref _windowInfo))
			{
				return HandleSDLError();
			}

			_winSurfaceCreateInfo.hwnd = _windowInfo.info.win.window;
			let hmodule = Windows.GetModuleHandleA(null);
			if (hmodule.IsInvalid)
			{
				return .Err;
			}
			_winSurfaceCreateInfo.hinstance = (Windows.HInstance)hmodule;
			
			if (Vulkan.Win32.CreateWin32SurfaceKHR(_instance, &_winSurfaceCreateInfo, null, &_surface) != .Success)
			{
				return HandleVulkanError();
			}
#else
			if (!SDL.Vulkan_CreateSurface(_window, (SDL.VkInstance)_instance, out _surface))
			{
				return HandleSDLError();
			}
#endif

#else
			_surface = SDL.GetWindowSurface(_window);
			if (_surface == null)
			{
				return HandleSDLError();
			}
			_glContext = SDL.GL_CreateContext(_window);
#endif
			_renderer = SDL.CreateRenderer(_window, -1, SDL.RendererFlags.Accelerated);
			if (_renderer == null)
			{
				return HandleSDLError();
			}
			SDL.SetRenderDrawColor(_renderer, 0xFF, 0xFF, 0xFF, 0xFF);


			let blitSurface = SDLImage.Load("Assets/squog.png");
			if (blitSurface == null)
			{
				return HandleSDLError();
			}
			_screenTexture = SDL.CreateTextureFromSurface(_renderer, blitSurface);
			if (_screenTexture == null)
			{
				return HandleSDLError();
			}

			return base.Initialize();
		}
		#endregion

		#endregion

		#region Private

		#region Members
		private SDL.Renderer* _renderer;
		private SDL.Texture* _screenTexture;
		private SDL.Window* _window;
		private SDL.SDL_SysWMinfo _windowInfo;
#if VULKAN
		private const int UNINITIALIZED_SURFACE = 0;
		private Vulkan.Win32.Win32SurfaceCreateInfoKHR _winSurfaceCreateInfo;
		private Vulkan.ApplicationInfo _appInfo;
		private Vulkan.Instance _instance;
		private Vulkan.InstanceCreateInfo _instanceCreateInfo;
#if BF_PLATFORM_WINDOWS
		private Vulkan.SurfaceKHR _surface;
#else
		private SDL.VkSurfaceKHR _surface;
#endif

#else
		private SDL.Surface* _surface;
		private SDL.SDL_GLContext _glContext;
#endif
		#endregion

		#region Member Methods
		private Result<bool> HandleSDLError()
		{
			Console.Error.WriteLine(scope:: String(SDL.GetError()));
			return .Err;
		}
#if VULKAN
		private Result<bool> HandleVulkanError()
		{
			Console.Error.WriteLine("Something went wrong with Vulkan");
			return .Err;
		}
#else
#endif
		#endregion

		#endregion

		public ~this()
		{
#if VULKAN
			if (_surface != UNINITIALIZED_SURFACE)
			{
				Vulkan.vkDestroySurfaceKHR(_instance, (Vulkan.SurfaceKHR)_surface, null);
			}
			Vulkan.vkDestroyInstance(_instance, null);
#else
			SDL.GL_DeleteContext(_glContext);
			SDL.FreeSurface(_surface);
			SDL.DestroyRenderer(_renderer);
#endif
			SDL.DestroyWindow(_window);
			SDL.Quit();
		}
	}
}
