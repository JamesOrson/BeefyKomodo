using System;

namespace Komodo.Core.Engine.Graphics
{
	public abstract class RenderWindow
	{
		public this(String title, uint width, uint height)
		{
			Title = title;
			Height = height;
			Width = width;
		}

		public String ScreenDeviceName => "Some screen";
		public String Title { get; set; }
		public uint Height { get; set; }
		public bool IsInitialized { get; protected set; }
		public uint Width { get; set; }

		public abstract void Blit();
		public virtual Result<bool> Initialize()
		{
			IsInitialized = true;
			return .Ok(IsInitialized);
		}
	}
}
