namespace Komodo.Core.Engine.Graphics
{
	public class Shader
	{
		public bool TextureEnabled { get; set; }
		public bool VertexColorEnabled { get; set; }
	}
}
