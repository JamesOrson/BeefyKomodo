using SDL2;
using System;
using Komodo.Core.Engine.Input;

namespace Komodo.Core.Engine.Event
{
	class SDLEventManager : EventManager
	{
		public override void Update()
		{
			base.Update();

			SDL.Event event;
			while (SDL.PollEvent(out event) != 0)
			{
				switch (event.type)
				{
					case SDL.EventType.Quit:
						Game.Instance.Exit();
						break;
					case SDL.EventType.KeyDown:
					    //Select surfaces based on key press
					    switch (event.key.keysym.scancode)
					    {
					        case SDL.Scancode.W:
					        	InputManager.[Friend]AddInputEvent(InputEvent("w"));
					        	break;
							case SDL.Scancode.A:
								InputManager.[Friend]AddInputEvent(InputEvent("a"));
								break;
							case SDL.Scancode.S:
								InputManager.[Friend]AddInputEvent(InputEvent("s"));
								break;
							case SDL.Scancode.D:
								InputManager.[Friend]AddInputEvent(InputEvent("d"));
								break;
							case SDL.Scancode.Escape:
								InputManager.[Friend]AddInputEvent(InputEvent("escape"));
								break;
					        default:
					        	InputManager.[Friend]AddInputEvent(InputEvent("other"));
					        	break;
					    }
						break;
					default:
						//System.Console.WriteLine("Some other event");
						break;
				}
			}
		}
	}
}
