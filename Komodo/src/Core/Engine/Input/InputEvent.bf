using System;

namespace Komodo.Core.Engine.Input
{
	public struct InputEvent
	{
		public this(String name)
		{
			Name = name;
		}

		public String Name;
	}
}
