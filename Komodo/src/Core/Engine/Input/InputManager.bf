using System.Collections;
namespace Komodo.Core.Engine.Input
{
	public static class InputManager
	{
		static this()
		{
			_events = new Queue<InputEvent>();
		}

		#region Public

		#region Static Methods
		public static void Update()
		{
			while (_events.Count > 0)
			{
				let event = _events.Dequeue();

				switch (event.Name)
				{
				    case "w":
				    	System.Console.WriteLine("Up");
				    	break;
					case "a":
						System.Console.WriteLine("Left");
						break;
					case "s":
						System.Console.WriteLine("Down");
						break;
					case "d":
						System.Console.WriteLine("Right");
						break;
					case "escape":
						Game.Instance.Exit();
						break;
				    default:
				    	System.Console.WriteLine("Other");
				    	break;
				}
			}
		}
		#endregion

		#endregion

		#region Private

		#region Static Members
		private static Queue<InputEvent> _events { get; set; }
		#endregion

		#region Static Methods
		private static void AddInputEvent(InputEvent event)
		{
			_events.Enqueue(event);
		}
		#endregion

		#endregion

		static ~this()
		{
			delete _events;
		}
	}
}
