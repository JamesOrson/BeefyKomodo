namespace Komodo.Lib.Math
{
	public struct Vector3
	{
	    #region Public

	    #region Constructors
	    public this(Vector3 vector) : this(vector.X, vector.Y, vector.Z)
	    {
	    }

	    public this(float x = 0f, float y = 0f, float z = 0f)
	    {
	        this.X = x;
			this.Y = x;
			this.Z = x;
	    }
	    #endregion

	    #region Public Members
	    public float X { get; }

	    public float Y { get; }

	    public float Z { get; }

	    #region Swizzling
	    public Vector2 XY => Vector2(X, Y);

	    public Vector2 XZ => Vector2(X, Z);

	    public Vector2 YX => Vector2(Y, X);

	    public Vector2 YZ => Vector2(Y, Z);

	    public Vector2 ZX => Vector2(Z, X);

	    public Vector2 ZY => Vector2(Z, Y);

	    public Vector3 XYZ => Vector3(X, Y, Z);

	    public Vector3 XZY => Vector3(X, Z, Y);

	    public Vector3 YXZ => Vector3(Y, X, Z);

	    public Vector3 YZX => Vector3(Y, Z, X);

	    public Vector3 ZXY => Vector3(Z, X, Y);

	    public Vector3 ZYX => Vector3(Z, Y, X);
	    #endregion

	    #endregion

	    #region Member Methods
	    public int GetHashCode()
	    {
	        return X.GetHashCode() + Y.GetHashCode() + Z.GetHashCode();
	    }
	    #endregion

	    #region Static Members
	    public static Vector3 One => Vector3(1f, 1f, 1f);

	    public static Vector3 Zero => Vector3();

	    public static Vector3 Up => Vector3(0f, 1f, 0f);

	    public static Vector3 Down => -Up;

	    public static Vector3 Right => Vector3(1f, 0f, 0f);

	    public static Vector3 Left => -Right;

	    public static Vector3 Back => Vector3(0f, 0f, 1f);

	    public static Vector3 Forward => -Back;
	    #endregion

	    #region Static Methods
	    public static int operator<=>(Vector3 left, Vector3 right)
		{
		    var cmp = left.X <=> right.X;
		    if (cmp != 0) {
				return cmp;
			}
		    cmp = left.Y <=> right.Y;
			if (cmp != 0) {
				return cmp;
			}
			return left.Z <=> right.Z;
		}

		public static Vector3 operator+(Vector3 left, Vector3 right) => Vector3.Add(left, right);

		public static Vector3 operator-(Vector3 left, Vector3 right) => Vector3.Subtract(left, right);

		public static Vector3 operator-(Vector3 vec)
		{
		    return .(-vec.X, -vec.Y, -vec.Z);
		}

		public static Vector3 Add(Vector3 left, Vector3 right)
		{
			return .(
				left.X + right.X,
				left.Y + right.Y,
				left.Z + right.Z
			);
		}

		public static Vector3 Subtract(Vector3 left, Vector3 right)
		{
		    return .(
				left.X - right.X,
				left.Y - right.Y,
				left.Z - right.Z
			);
		}
	    #endregion

	    #endregion
	}
}
