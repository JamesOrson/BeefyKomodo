using Komodo.Core.ECS.Components;

namespace Sample.Behaviors
{
	public class TestBehavior : BehaviorComponent
	{
		public float acc { get; set; }

		public override void Update(float delta)
		{
			acc += delta;
			int x = 0;
			for (var i = 0; i < 1000; ++i)
			{
				x += i;
			}
			
			//System.Console.WriteLine(this.ID);
			if (acc > 2f)
			{
				//Game.Exit();
			}
		}
	}
}
