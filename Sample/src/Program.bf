using System;
using Komodo;
using Sample.Behaviors;
using Komodo.Core.ECS;

namespace Sample
{
	class Program
	{
		public static int Main(String[] args)
		{
			let game = scope:: Game();
			let entity = new Entity();
			game.AddEntity(entity);
			for (var i = 0; i < 10; ++i)
			{
				let component = new TestBehavior();
				entity.AddComponent(component);
			}

			game.Run();
			return 0;
		}
	}
}